package com.dwi.weigher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       Button btnMulai = (Button) findViewById(R.id.btn_mulai);
       btnMulai.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent mulai = new Intent(MainActivity.this, mulai.class);
               startActivity(mulai);
           }

       });
        Button btn_Infokes = (Button) findViewById(R.id.btn_infokes);
        btn_Infokes.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View v){
                Intent Infokes = new Intent(MainActivity.this,infokes2.class);
                startActivity(Infokes);
            }
        });

        Button btn_lbs = (Button) findViewById(R.id.btn_lbs);
        btn_lbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vegetarianrestoran = new Intent(MainActivity.this, vegetarianrestoran.class);
                startActivity(vegetarianrestoran);
            }
        });
        Button btn_bantuan =(Button) findViewById(R.id.btn_bantuan);
        btn_bantuan.setOnClickListener(new View.OnClickListener(){
            @Override
        public void onClick (View v){
                Intent help = new Intent(MainActivity.this, help.class);
                startActivity(help);
            }
        });
        Button btn_riwayat = (Button) findViewById(R.id.btn_riwayat);
        btn_riwayat.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               Intent Riwayathitung = new Intent(MainActivity.this, Riwayathitung.class);
                                               startActivity(Riwayathitung);
                                           }
                                       });
        Button btn_tolong = (Button) findViewById(R.id.btn_tolong);
        btn_tolong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bblebih = new Intent(MainActivity.this, bblebih.class);
                startActivity(bblebih);
            }
        });

                Button btn_keluar = (Button) findViewById(R.id.btn_beranda);
        btn_keluar.setOnClickListener(new View.OnClickListener(){
            @Override
        public void onClick (View v){
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory( Intent.CATEGORY_HOME );
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            }
        });
    }}
