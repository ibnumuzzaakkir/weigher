package com.dwi.weigher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class infokes2 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infokes2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button btn_vitamin = (Button) findViewById(R.id.btn_vitamin);
        btn_vitamin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vitamin = new Intent(infokes2.this, vitamin.class);
                startActivity(vitamin);
            }
        });
        Button btn_karbo = (Button) findViewById(R.id.btn_karbo);
        btn_karbo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent karbohidrat = new Intent(infokes2.this, karbohidrat.class);
                startActivity(karbohidrat);
            }
        });
        Button btn_mineral = (Button) findViewById(R.id.btn_mineral);
        btn_mineral.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent mineral = new Intent (infokes2.this, mineral.class);
                startActivity(mineral);
            }
        });

        Button btn_prt = (Button) findViewById(R.id.btn_prt);
        btn_prt.setOnClickListener(new View.OnClickListener(){
            @Override
        public void onClick(View v) {
                Intent prt = new Intent (infokes2.this, prt.class);
                startActivity(prt);
            }
        });
        Button btn_serat = (Button) findViewById(R.id.btn_serat);
        btn_serat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent serat = new Intent (infokes2.this, serat.class);
                startActivity(serat);
            }
        });
    }}