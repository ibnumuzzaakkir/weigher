package com.dwi.weigher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
public class adapter_restoran extends RecyclerView.Adapter<adapter_restoran.ViewHolderRestoran> {

private List<model_restoran>restoranList;
private Context context;
public adapter_restoran(List<model_restoran>model_restoranListList, Context context) {
    this.restoranList = model_restoranListList;
    this.context = context;
}
    @Override
    public ViewHolderRestoran onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.list_restoran, parent, false);

        return new ViewHolderRestoran(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderRestoran holder, int position){

        final model_restoran modelRestoran = restoranList.get(position);
        holder.namarestoran.setText(modelRestoran.getNamaRestoran());
        holder.noTelp.setText(modelRestoran.getNoTelp());
        holder.gambarRs.setImageResource(modelRestoran.getGambarResto());
        holder.setClickListener(new itemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0?q="+modelRestoran.getLatResto()+","+modelRestoran.getLongResto()+"("+modelRestoran.getNamaRestoran()+")"));
                view.getContext().startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return restoranList.size();
    }

    static class ViewHolderRestoran extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView namarestoran;
        TextView noTelp;
        ImageView gambarRs;
        private itemClickListener clickListener;

        public ViewHolderRestoran(View itemView) {
            super(itemView);
            namarestoran= (TextView) itemView.findViewById(R.id.namaRs);
            noTelp= (TextView) itemView.findViewById(R.id.noTelp);
            gambarRs= (ImageView) itemView.findViewById(R.id.iconRs);
            itemView.setOnClickListener(this);
        }
    public void setClickListener(itemClickListener itemClickListener) {
        this.clickListener= itemClickListener;
    }
    @Override
    public void onClick(View view)
    {
        int position = getAdapterPosition();
        clickListener.onClick(view, position);
    }
}
}



















