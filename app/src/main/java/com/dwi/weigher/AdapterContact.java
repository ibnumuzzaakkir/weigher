package com.dwi.weigher;

import android.content.Context;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ibnumuzzakkir on 7/16/2016.
 */
public class AdapterContact extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {
    public Context context;
    private ArrayList<ContactModel> mContact = new ArrayList<>();
    public AdapterContact(Context context){
        this.context = context;

    }
    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_user,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContactViewHolder contactViewHolder = (ContactViewHolder) holder;
        ContactModel contactModel = mContact.get(position);
        Log.d("datas", Integer.toString(mContact.size()));
        contactViewHolder.tvID.setText("Id : " + contactModel.getID());
        contactViewHolder.tvNama.setText("Nama :  " + contactModel.getNama());
        contactViewHolder.tvBerat.setText("Berat : " +contactModel.getBerat());
        contactViewHolder.tvStatus.setText("Status : "+contactModel.get_statusbb());;
        contactViewHolder.tvBBI.setText("BBI : " +contactModel.get_bbideal());
    }

    @Override
    public int getItemCount() {
        return mContact.size();
    }
    public void setContactList(ArrayList<ContactModel> listContact) {
        this.mContact = listContact;
        //update the adapter to reflect the new set of movies
        notifyDataSetChanged();
    }
    static class ContactViewHolder  extends RecyclerView.ViewHolder {
        TextView tvID, tvNama, tvBerat, tvStatus, tvBBI;
        Context context;

        public ContactViewHolder(View inflate) {
            super(inflate);
            context = inflate.getContext();
            tvID = (TextView) inflate.findViewById(R.id.tvID);
            tvNama = (TextView) inflate.findViewById(R.id.tvNama);
            tvBerat = (TextView) inflate.findViewById(R.id.tvBerat);
            tvStatus = (TextView) inflate.findViewById(R.id.tvStatus);
            tvBBI = (TextView) inflate.findViewById(R.id.tvBBI);
        }
    }}
