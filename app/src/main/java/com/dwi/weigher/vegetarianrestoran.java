package com.dwi.weigher;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import java.util.ArrayList;
import java.util.List;

public class vegetarianrestoran extends AppCompatActivity {
    private RecyclerView recyclerView;
    private String[] namaRestoran;
    private String[]  noTelp;
    private int[]  gambarResto;
    private double[]  latResto;
    private double[] longResto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vegetarianrestoran);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//array Data
        namaRestoran= new String[]{"Ananda’s vegetarian restaurant and cafe", "Pranita rasa vegetarian","Dharma Kitchen Jakarta Barat", "Vegan House", "Vmad vegetarian madness",
                "Oma Vegetarian","Grassland vegetarian restourant","Warveg Anjali","Vegetarian Xiang he","Happy Healthy Yummy",".Burgreens","Dharma Kitchen","Vegetus Vegetarian","Loving Hut Jakarta Timur","Loving Hut Exspress"};
        noTelp= new String[]{"021-6628081","021-6679085","021-29200018", "021-91729759","021-3903533 ","0818-159-191","021-29414533","021-55955431","021-63866049","021-4406136","0859-5966-2888","021-23580198","021-6610594","021-85919168","021-32750501"};
        gambarResto= new int[]{R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran,R.drawable.restoran};
        latResto= new double[]{-6.121466, -6.116538 ,-6.177186,-6.136270,-6.187589,-6.230531,-6.161504,-6.133981, -6.164258, -6.148575, -6.238051, -6.195268, -6,122531, -6.218264, -6.199559 };
        longResto= new double[]{106.782920, 106.779226, 106.791268, 106.724297, 106.824231, 106.825559, 106.781482, 106.718949, 106.815438, 106.942190, 106.851947, 106.820780, 106.783157, 106.868399, 106.822456};
//RecycleView Start
        recyclerView= (RecyclerView) findViewById(R.id.List_restoran);
        recyclerView.setLayoutManager(new GridLayoutManager(vegetarianrestoran.this,2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//Populate RecycleView
        adapter_restoran adp_restoran = new adapter_restoran(createList(),getApplicationContext());
        recyclerView.setAdapter(adp_restoran);
    }
    private List<model_restoran>createList() {

        List<model_restoran> result = new ArrayList();
        for (int i =0; i <10; i++) {
            model_restoran model_restoran = new model_restoran();
            model_restoran.setNamaRestoran(namaRestoran[i]);
            model_restoran.setNoTelp (noTelp[i]);
            model_restoran.setGambarResto(gambarResto[i]);
            model_restoran.setLatResto(latResto[i]);
            model_restoran.setLongResto(longResto[i]);
            result.add(model_restoran);
        }

        return result;
    }

    private void gambarRs(int gambarRs) {
    }
}
