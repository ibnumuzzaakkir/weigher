package com.dwi.weigher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class mulai extends AppCompatActivity {
   private double tinggi;
    private int berat;
    private  double hasil;
    private int JK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mulai);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final EditText edNama = (EditText)findViewById(R.id.ednama);
       final  EditText edTinggi = (EditText)findViewById(R.id.edtinggi);
        final EditText edBerat = (EditText)findViewById(R.id.edberat);
        final Button btn_hitung =(Button) findViewById(R.id.btn_hitung);
        final RadioButton radiobtn_laki=(RadioButton)findViewById(R.id.radiobtn_laki);
        final RadioButton radiobtn_prm=(RadioButton)findViewById(R.id.radioBtn_prm);
        btn_hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //object baru dari hitungan
                hitungan ht = new hitungan();

                tinggi= Double.parseDouble(edTinggi.getText().toString());
                berat= Integer.parseInt(edBerat.getText().toString());
                if(radiobtn_laki.isChecked()){
                    ht.setJenisKelamin(1);
                    Log.i("jenis", Integer.toString(ht.getJenisKelamin()));
                }else if(radiobtn_prm.isChecked()){
                    ht.setJenisKelamin(2);
                }
                //rumus baru
                double tinggiKurangSeratus = tinggi - 100;
                double hasilBB = (tinggiKurangSeratus - (0.1*tinggiKurangSeratus));
                ht.setHasilBB(hasilBB);
                //rumus lama
                double tinggicuy = (tinggi/100);
                double temp = Math.pow(tinggicuy,2);
                Log.i("tinggi",Double.toString(tinggicuy));
                Log.i("temp",Double.toString(temp));
                hasil = berat/temp;
                ht.setHasil(hasil);
                Intent help = new Intent(mulai.this,hasil.class);
                help.putExtra("nama",edNama.getText().toString());
                help.putExtra("berat",edBerat.getText().toString());
                startActivity(help);
            }
        });

    }

}
