package com.dwi.weigher;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "db_weigher";

    // Contacts table name
    private static final String TABLE_WEIGHER = "weigher";

    // Contacts Table Columns names
    private static final String KEY_NAMA= "tinggi";
    private static final String KEY_BERAT = "berat";
    private static final String KEY_ID = "id";
    private static final String KEY_STATUSBERATBADAN = "statusbb";
    private static final String KEY_BBIDEAL = "bbideal";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_WEIGHER_TABLE = "CREATE TABLE " + TABLE_WEIGHER + "("
                + KEY_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+ KEY_NAMA + " TEXT,"
                + KEY_BERAT + " INTEGER,"+ KEY_STATUSBERATBADAN + " TEXT,"
                + KEY_BBIDEAL + " TEXT" + ")";
        db.execSQL(CREATE_WEIGHER_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEIGHER);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    void addContact(ContactModel contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAMA,contact.getNama());
        values.put(KEY_BERAT, contact.getBerat()); // Contact berat
        values.put(KEY_STATUSBERATBADAN, contact.get_statusbb());
        values.put(KEY_BBIDEAL, contact.get_bbideal());
        // Inserting Row
        db.insert(TABLE_WEIGHER, null, values);
        db.close(); // Closing database connection
    }
    void deleteAllContact(){
        SQLiteDatabase db = this.getReadableDatabase();

       db.execSQL("DELETE FROM " + TABLE_WEIGHER);

    }

    // Getting single contact
    ContactModel getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_WEIGHER, new String[] { KEY_ID,
                        KEY_BERAT, KEY_STATUSBERATBADAN,KEY_BBIDEAL}, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ContactModel contact = new ContactModel(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4) );
        // return contact
        return contact;
    }

    // Getting All Contacts
    public ArrayList<ContactModel> getAllContacts() {
        ArrayList<ContactModel> contactList = new ArrayList<ContactModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WEIGHER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ContactModel contact = new ContactModel();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setNama(cursor.getString(1));
                contact.set_berat(cursor.getString(2));
                contact.set_statusbb(cursor.getString(3));
                contact.set_bbideal(cursor.getString(4));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    // Updating single contact
    public int updateContact(ContactModel contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_BERAT, contact.getBerat());
        values.put(KEY_STATUSBERATBADAN, contact.get_statusbb());
        values.put(KEY_BBIDEAL, contact.get_bbideal());
        // updating row
        return db.update(TABLE_WEIGHER, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }

    // Deleting single contact
    public void deleteContact(ContactModel contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WEIGHER, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_WEIGHER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}