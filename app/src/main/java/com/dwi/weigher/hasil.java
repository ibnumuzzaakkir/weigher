package com.dwi.weigher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class hasil extends AppCompatActivity {
  private double hasilHitungan;
    public String hasil2,nama,berat,hasilDua;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);
        Intent intent = getIntent();
        nama = intent.getStringExtra("nama");
        berat = intent.getStringExtra("berat");
        DecimalFormat df = new DecimalFormat("#.##");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        hitungan ht = new hitungan();
      TextView txthasil= (TextView)findViewById(R.id.hasil);
        EditText txtHasilBB = (EditText) findViewById(R.id.bbi);
        final TextView txtStatus = (TextView)findViewById(R.id.status);
        Button btnLanjut = (Button) findViewById(R.id.btn_lanjut);
        //hasil bb
        hasilHitungan = ht.getHasil();
        txtHasilBB.setText(Double.toString(ht.getHasilBB()));
        hasilDua = txtHasilBB.getText().toString();
       //hitungan yang sebelumnya
        Double berattemp = Double.parseDouble(df.format(hasilHitungan));
        switch (ht.getJenisKelamin()){
            case 1:
                Log.i("temp2", String.valueOf(berattemp));
                if(berattemp < 18.00){
                    txtStatus.setText("Berat Badan Kurang");
                    hasil2 = "Berat Badan Kurang";
                }else if(((berattemp >= 18.00)&&(berattemp < 25.00))){
                    txtStatus.setText("Normal");
                    hasil2 = "Normal";
                }else if(((berattemp >= 25.00)&&(berattemp <27.00))){
                    txtStatus.setText("Berat Badan Lebih");
                    hasil2 = "Berat Badan Lebih";
                }else{
                    hasil2 = "Obesitas";
                    txtStatus.setText("Obesitas");
                }
                break;
            case 2:
                if(berattemp< 17){
                    hasil2 = "Berat Badan Kurang";
                    txtStatus.setText("Berat Badan Kurang");
                }else if(((berattemp>=17)&&(berattemp<23))){
                    txtStatus.setText("Normal");
                    hasil2 = "Normal";
                }else if(((berattemp>=23)&&(berattemp<27))){
                    txtStatus.setText("Berat Badan Lebih");
                    hasil2 = "Berat Badan Lebih";
                }else{
                    hasil2 = "Obesitas";
                    txtStatus.setText("Obesitas");
                }
                break;

        }
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        db.addContact(new ContactModel(nama,berat,hasil2,hasilDua));
        txthasil.setText(Double.toString(Double.parseDouble(df.format(hasilHitungan))));
        btnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtStatus.getText().toString().equals("Berat Badan Kurang")){
                    Intent i = new Intent(hasil.this, NaikBB.class);
                    startActivity(i);
                }else if(txtStatus.getText().toString().equals("Normal")) {
                    Intent i = new Intent(hasil.this, NormalBB.class);
                    startActivity(i);
                }else if (txtStatus.getText().toString().equals("Berat Badan Lebih")){
                    Intent i = new Intent(hasil.this, TurunBB.class);
                    startActivity(i);
                }else if(txtStatus.getText().toString().equals("Obesitas")){
                    Intent i = new Intent(hasil.this, TurunBB.class);
                    startActivity(i);
                }

            }
        });
    }
}
