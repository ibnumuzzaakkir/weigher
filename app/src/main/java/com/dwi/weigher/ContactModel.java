package com.dwi.weigher;

/**
 * Created by ibnumuzzakkir on 7/16/2016.
 */
public class ContactModel {

    //private variables
    int _id;
    String _statusbb;
    String _berat;
    String _bbideal;
    String _nama;

    // Empty constructor
    public ContactModel() {

    }

    // constructor
    public ContactModel(int _id, String _nama, String _berat,String _statusbb, String _bbideal) {
       this._nama = _nama;
        this._id = _id;
        this. _berat = _berat;
        this._statusbb = _statusbb;
        this._bbideal = _bbideal;
    }
    public ContactModel(String _nama, String _berat,String _statusbb, String _bbideal) {
        this._nama = _nama;
        this._berat = _berat;
        this._statusbb = _statusbb;
        this._bbideal = _bbideal;
    }
    //get nama
    public String getNama() {return this._nama;}
    // set nama
    public void setNama(String _nama) {
        this._nama = _nama;
    }
    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting berat
    public String getBerat() {
        return this._berat;
    }

    // setting Berat
    public void set_berat(String _berat) {
        this._berat = _berat;
    }

    public String get_statusbb() {return this. _statusbb;}
    //setting status bb
    public void set_statusbb (String _statusbb) {this._statusbb = _statusbb;}
    //getting bbideal
    public String get_bbideal() {return this._bbideal;}
    //setting bbideal
    public void set_bbideal (String _bbideal) {this._bbideal = _bbideal;}


}